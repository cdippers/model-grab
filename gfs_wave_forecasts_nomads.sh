#!/bin/bash
#- Archive GFS Wave Model from NOMADS data server:
#- https://nomads.ncep.noaa.gov
#- Usage: setup as hourly cron on syntool_acq

##- Initialize
dset_name="NCEP_GFS_WAVE_MODEL_FORECASTS"
out_path="/mnt/syntool/raw/${dset_name}/"
log="/mnt/syntool/logs/${dset_name}/wget.out"

##- Get a list of regions to process
regions=`cat $out_path/files/gfswave.regions`

get_timestamp () {
    local arg=${1:-0}
    local timestamp=`date -u -d "${arg} hours ago" +"%Y-%m-%dT%H:00:00Z"`
    echo $timestamp
}

##- Figure out latest reference time (model runtime)
current_hr=`date -u "+%H"`
#ref_hr=$(( $current_hr - $current_hr % 6 ))
#base=`date -u "+%Y-%m-%d "$ref_hr`

#echo $current_hr
#echo $ref_hr
#echo $base

##- Get latest reference time 20210811 0600
src_url="https://thredds.ucar.edu/thredds/catalog/grib/NCEP/WW3/Global/latest.html"
ref_time="$(curl -s $src_url | grep ".grib2" | \
	awk -F ".grib2" '{print $1}' | \
	awk -F "/" '{print $NF}' | \
	awk -F "_" '{print($3" "$4)}' )"
#echo $ref_time
yyyymmdd=`echo $ref_time | awk '{print $1}'`
YYYY=`echo $yyyymmdd | awk '{print substr($1,0,4)}'`
MM=`echo $yyyymmdd | awk '{print substr($1,5,2)}'`
DD=`echo $yyyymmdd | awk '{print substr($1,7,2)}'`
ref_hr=`echo $ref_time | awk '{print substr($2,0,2)}'`

echo "Latest data @ "$ref_time ":"
#echo $ref_time
#echo $yyyymmdd
#echo $ref_hr

##- Setup output directory
out_dir=$out_path$YYYY/$MM/$DD/$ref_hr
mkdir -p $out_dir
echo "Output directory: "$out_dir

##- Setup source urls from nomads
gfs_url="https://nomads.ncep.noaa.gov/pub/data/nccf/com/gfs/prod/gfs."$yyyymmdd/$ref_hr/"wave/gridded/"

for region in ${regions[@]}
do
    ##- Iterate over 0-384 records spaced @ 1-hr interval
    for hr in $(seq -f "%03g" 0 384)
    do
        ##- Grab the grib file
        fname="gfswave.t"$ref_hr"z."$region".f"$hr".grib2"
        wget -q -O $out_dir/$fname $gfs_url$fname 
        ##- Grab the grib idx file
        wget -q -O $out_dir/$fname".idx" $gfs_url$fname".idx" 
    done
done

##- Now delete any zero-byte files

