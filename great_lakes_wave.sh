#!/bin/bash
##- Grab Wave data from the Great Lakes Wave (GLWU) forecast system, 
## Currently only grabs 4-runs: 01,07,13,19
##- late run forced with NDFD winds 

model_name="GREAT_LAKES_WAVE_MODEL"
out_path="/mnt/syntool/raw/${model_name}/"
out_log=$out_path"greatlakes_ftp_log.txt"

##- Figure out latest reference time (model runtime)
current_hr=`date -u "+%H"`
#yyyymmdd=`date -u "+%Y%m%d"`
#ref_hr=`printf "%02d" $ref_hr`
#echo $current_hr
#echo $ref_hr

yyyymmdd=`date -u "+%Y%m%d"`
yyyymmdd="20220106"

yyyymm=`echo $yyyymmdd | awk '{print(substr($0,0,6))}'`
ref_hrs=("01" "07" "13" "19")

home=$out_path$yyyymm/
if [ ! -d $home ]; then
    /bin/mkdir $home
fi

for hr in ${ref_hrs[@]}
do
    #url="https://nomads.ncep.noaa.gov/pub/data/nccf/com/nwps/prod/"$r"."$yyyymmdd"/"
    url="https://nomads.ncep.noaa.gov/cgi-bin/filter_glwu.pl?file=glwu.grlc_2p5km.t"$hr"z.grib2&all_lev=on&var_DIRPW=on&var_HTSGW=on&var_PERPW=on&var_UGRD=on&var_VGRD=on&var_WDIR=on&var_WIND=on&var_WVDIR=on&var_WVHGT=on&var_WVPER=on&leftlon=-92.2&rightlon=-84.2&toplat=49.0&bottomlat=46.4&dir=%2Fglwu."$yyyymmdd
    fname='glwu_'$yyyymmdd$hr'.grib'
    out=$home$fname
    echo "Grabbing $url: " $out

    wget $url -q -O $out
done
##- Cleanup zero byte files
find $out_path -size 0 -delete

